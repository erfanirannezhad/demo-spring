package com.example.demo.controller

data class Greeting(val id: Long, val content: String)