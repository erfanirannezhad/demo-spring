package com.example.demo.controller

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Controller
class DemoController {

    @RequestMapping("/")
    fun demo (model : Model) : String {
        model["title"] = "blog"
        return "demo"
    }
}